# bank2ynab

Bank2Ynab es una aplicacion que permite transformar el archivo de movimientos de tu cuenta de tu banco (por ahora tan solo ING) a un formato CSV adaptado a YNAB.

## Funcionalidades

 - Busca en los movimientos y añade los "payees" personalizados. De esta forma YNAB categoriza automáticamente los movimientos.
 
## Bancos disponibles

 - ING españa
 
## Descarga

Ver la sección [releases](https://gitlab.com/edavidfs/bank2ynab/-/releases)

## Consideraciones iniciales
Es fundamental que antes de realizar la importación de los datos en YNAB  tengamos la cuenta "Reconciliada", es decir verificar que hasta la fecha del último movimiento el balance sea correcto, y tengas todas las operaciones 'cleared' con el candado.
De esta forma si no nos convence la importación de los datos siempre es posible seleccionar las operaciones añadidas y eliminarlas.
   
## Como usar la app

Su uso es bastante sencillo:

 1. Descarga el archivo xls desde ING
 2. Verificar que las Cuentas estan **Reconciliadas** y todas las operaciones en estado *'Cleared'*
 3. Abre la aplicación bank2ynab, en la pestaña *Datos* pulsa en el botón **Cargar XLS**
 4. Selecciona el archivo xls y Acepta
 5. En ese instante se procesa el archivo y el resultado se puede ver en la tabla del **Archivo de salida**
 6. Para añadir los Payees, pasamos a la pestaña *Payess*
 7. Se escribe el nombre del Payee deseado(por ejemplo Mercadona) en el campo de texto encima del boton **Añadir Payee**
 8. Pulsamos sobre el botón **Añadir Payee**.
 9. Seleccionamos el nombre del Payee en la tabla de la izquierda.
 10. En el campo de texto sobre el botón **Añadir Palabra Clave** escribimos la palabra que se buscará en el campo descripción(en el caso de ejemplo Mercadona)
 11. Pulsamos sobre el botón **Añadir Palabra Clave**.
 12. Volvemos a la pestaña **Datos** y pulsamos el botón inferior **Procesar Archivo de Entrada** para volver a realizar la busqueda.
 13. Cuando deseemos exportar el archivo CVS pulsamos sobre el boton **Exportar**
 14. Cargamos el archivo CVS a YNAB.
 15. Verificamos que la importación se ha realizado correctamente.
 