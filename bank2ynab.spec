# -*- mode: python ; coding: utf-8 -*-

block_cipher = None

import distutils
if distutils.distutils_path.endswith('__init__.py'):
    distutils.distutils_path = os.path.dirname(distutils.distutils_path)


a = Analysis(['bank2ynab/bank2ynab.py'],
             pathex=['/Users/edavidfs/Documents/Repositorios/Personales/bank2ynab'],
             binaries=[],
             datas=[],
             hiddenimports=['pkg_resources.py2_warn',
             'sqlalchemy.ext.baked'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='bank2ynab',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          icon = '.resources/dollar-icon.icns',
          runtime_tmpdir=None,
          console=False )
app = BUNDLE(exe,
             name='bank2ynab.app',
             icon='resources/dollar-icon.icns',
             bundle_identifier=None,
             info_plist={
                'NSPrincipleClass': 'NSApplication',
                'NSAppleScriptEnabled': False,
                'NSHighResolutionCapable': True,
                'CFBundleDocumentTypes': [
                {
                    'CFBundleTypeName': 'My File Format',
                    'CFBundleTypeIconFile': 'resources/dollar-icon.icns',
                    'LSItemContentTypes': ['com.example.myformat'],
                    'LSHandlerRank': 'Owner'
                }
            ]
        })
