from app.models.db.database import DataBase, Payee, PayeeKeyword
from app.mainwindow import MainWindow
from app.models.xls2ynab import Xls2Ynab


from pandas import read_excel
import logging
logger = logging.getLogger(__name__)


class AppController:

    def __init__(self, mainwindow: MainWindow):
        self.db = DataBase()
        self.mainwindow = mainwindow
        self.ynab_data = None

        self.connect_signals()

        self.show_payees()

    def connect_signals(self):
        self.mainwindow.s_add_payee.connect(self.add_payee)
        self.mainwindow.s_add_payee_keyword.connect(self.add_payee_keyword)
        self.mainwindow.s_show_payee_keyword.connect(self.show_payee_keywords)
        self.mainwindow.s_delete_payee_keyword.connect(self.delete_payee_keyword)
        self.mainwindow.s_delete_payee.connect(self.delete_payee)
        self.mainwindow.s_process_data_from_excel.connect(self.process_data_from_excel)
        self.mainwindow.s_export_data_to_ynab.connect(self.export_data_to_ynab)

    def add_payee(self, payee):
        new_payee = Payee(name = payee)
        self.db.session.add(new_payee)
        self.db.session.commit()
        self.show_payees()

    def delete_payee(self, payee_name):
        payee = self.db.session.query(Payee).filter(Payee.name == payee_name).first()
        logger.debug(f"Delete Payee: {payee.name}")
        if payee:
            payee_keywords = self.db.session.query(PayeeKeyword).filter(PayeeKeyword.payee_id == payee.id).all()
            for keyword in payee_keywords:
                logger.debug(f"Delete Keyword: {keyword.keyword}")
                self.db.session.delete(keyword)

        self.db.session.delete(payee)
        self.db.session.commit()
        self.show_payees()

    def add_payee_keyword(self, payee_name, payee_keyword):
        payee = self.db.session.query(Payee).filter(Payee.name == payee_name).first()
        new_payee_keyword = PayeeKeyword(keyword = payee_keyword,
                                         payee_id = payee.id)
        self.db.session.add(new_payee_keyword)
        self.db.session.commit()
        self.show_payee_keywords(payee.name)

    def delete_payee_keyword(self, payee_name, payee_keyword_name):
        payee_keyword = self.db.session.query(PayeeKeyword).filter(PayeeKeyword.keyword == payee_keyword_name).first()
        logger.debug(f"Delete Payee keyword: {payee_keyword.keyword}")
        self.db.session.delete(payee_keyword)
        self.db.session.commit()
        self.show_payee_keywords(payee_name)

    def process_data_from_excel(self, file_path):
        df_excel = read_excel(file_path, skiprows=5)
        logger.info('Num of entry: ' + str(df_excel.shape[0]))

        self.ynab_data = Xls2Ynab(file_xls=file_path, database=self.db)

        self.mainwindow.show_data_to_convert(self.ynab_data)

    def export_data_to_ynab(self, ynab2xls: Xls2Ynab, file_cvs_path):
        self.ynab_data = ynab2xls
        self.ynab_data.data.set_index('Date', inplace=True)
        self.ynab_data.data.to_csv(file_cvs_path)

    def show_payees(self):
        payees = self.db.session.query(Payee).all()
        payees_list = []
        for payee in payees:
            payees_list.append(payee)
            logger.debug(f"Payees: {payee.name}")

        logger.debug(f"Payees: {payees_list}")
        self.mainwindow.show_payee_list(payees_list)

    def show_payee_keywords(self, payee_name):
        payee = self.db.session.query(Payee).filter(Payee.name == payee_name).first()
        payee_keyword_list = []
        if payee:
            logger.debug(f"Payee found: {payee.name}")
            payee_keywords = self.db.session.query(PayeeKeyword).filter(PayeeKeyword.payee_id == payee.id).all()
            for keyword in payee_keywords:

                payee_keyword_list.append(keyword)
            logger.debug(f"Payee keyword: {payee_keyword_list}")
            self.mainwindow.show_payee_keyword_list(payee_keyword_list)
        else:
            logger.debug(f"Payee has not keywords")





