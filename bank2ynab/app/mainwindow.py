
import logging
from PySide2.QtWidgets import QMainWindow, QFileDialog, QTableWidgetItem
from PySide2.QtCore import Signal

from app.ui.ui_mainwindow import Ui_MainWindow
from app.models.pandas2qt import Pandas2Qt
from app.models.xls2ynab import Xls2Ynab

logger = logging.getLogger(__name__)


class MainWindow(QMainWindow):

    s_add_payee = Signal(object)
    s_add_payee_keyword = Signal(object, object) # payee_name, Payee_keyword

    s_delete_payee_keyword = Signal(object, object) # payee_name, Payee_keyword
    s_delete_payee = Signal(object)  # payee_name
    s_show_payee_keyword = Signal(object)

    s_process_data_from_excel = Signal(object)

    s_export_data_to_ynab = Signal(object,object) # data, filename

    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ynab_data_model = None

        self.connect_signals()

    def connect_signals(self):
        self.ui.pushButton_load_data.clicked.connect(self.load_file)
        self.ui.pushButton_process_data.clicked.connect(self.process_data)
        self.ui.pushButton_add_payee.clicked.connect(self.add_payee)
        self.ui.pushButton_add_payee_keyword.clicked.connect(self.add_payee_keyword)
        self.ui.pushButton_delete_payee_keyword.clicked.connect(self.delete_payee_keyword)
        self.ui.pushButton_delete_payee.clicked.connect(self.delete_payee)

        self.ui.pushButton_export_data.clicked.connect(self.export_data)

        self.ui.tableWidget_payees.cellClicked.connect(self.payee_clicked)

    def select_input_file(self):
        logger.debug("Select input file")
        input_file = QFileDialog.getOpenFileName(self, caption="Select xls file", selectedFilter="*.xls")
        file_name = input_file[0]
        self.ui.lineEdit_input_file.setText(file_name)
        logger.debug(f"File selected: {file_name}")

    def load_file(self):
        self.select_input_file()
        self.process_data()

    def process_data(self):
        file_path = self.ui.lineEdit_input_file.text()
        if file_path is not "":
            logger.debug(f"Load Data from: {file_path}")
            self.s_process_data_from_excel.emit(file_path)

    def select_output_file(self):
        logger.debug("Select output file")
        output_file = QFileDialog.getSaveFileName(self, caption="Select csv file",
                                                  dir=self.ynab_data_model.output_file_name,
                                                  filter="*.csv")
        file_name = output_file[0]
        self.ui.lineEdit_output_file.setText(file_name)
        logger.debug(f"File selected: {file_name}")
        return output_file[0]

    def export_data(self):
        output_file = self.select_output_file()
        if output_file is not "":
            logger.debug(f"Export Data to: {output_file}")
            self.s_export_data_to_ynab.emit(self.ynab_data_model, output_file)

    def add_payee(self):
        new_payee = self.ui.lineEdit_new_payee.text()
        logger.debug(f"Add Payee: {new_payee}")
        self.s_add_payee.emit(new_payee)
        self.ui.lineEdit_new_payee.clear()

    def add_payee_keyword(self):
        payee = self.ui.tableWidget_payees.currentItem().text()
        if payee:
            new_payee_keyword = self.ui.lineEdit_new_payee_keyword.text()
            logger.debug(f"Add Payee keyword: {new_payee_keyword} to {payee}")
            self.s_add_payee_keyword.emit(payee, new_payee_keyword)
            self.ui.lineEdit_new_payee_keyword.setText("")
        else:
            logger.info("Select a Payee before")

    def delete_payee_keyword(self):
        payee = self.ui.tableWidget_payees.currentItem().text()
        if payee:
            payee_keyword_name = self.ui.tableWidget_payee_keywords.currentItem().text()
            logger.debug(f"Delete Payee keyword: {payee_keyword_name} from {payee}")
            self.s_delete_payee_keyword.emit(payee, payee_keyword_name)
        else:
            logger.info("Select a Payee before")

    def delete_payee(self):
        payee = self.ui.tableWidget_payees.currentItem().text()
        if payee:

            logger.debug(f"Delete Payee: {payee}")
            self.s_delete_payee.emit(payee)
        else:
            logger.info("Select a Payee before")

    def show_payee_list(self, payees_list):
        index = 0
        self.ui.tableWidget_payees.setRowCount(len(payees_list))
        self.ui.tableWidget_payees.setColumnCount(2)

        for payee in payees_list:
            self.ui.tableWidget_payees.setItem(index, 0, QTableWidgetItem(str(payee.id)))
            self.ui.tableWidget_payees.setItem(index, 1, QTableWidgetItem(payee.name))
            index += 1

    def show_payee_keyword_list(self, payee_keyword_list):
        #self.ui.textEdit_payees.setText( ", ".join(payees_list))
        index = 0
        self.ui.tableWidget_payee_keywords.setRowCount(len(payee_keyword_list))
        self.ui.tableWidget_payee_keywords.setColumnCount(2)

        for keyword in payee_keyword_list:
            self.ui.tableWidget_payee_keywords.setItem(index, 0, QTableWidgetItem(str(keyword.id)))
            self.ui.tableWidget_payee_keywords.setItem(index, 1, QTableWidgetItem(keyword.keyword))
            index += 1

    def show_data_to_convert(self, xls2ynab: Xls2Ynab ):
        self.ynab_data_model = xls2ynab
        self.data_xls_model = Pandas2Qt(xls2ynab.data_xls)
        self.data_ynab_model = Pandas2Qt(xls2ynab.data)

        self.ui.tableView_xls_data.setModel(self.data_xls_model)
        self.ui.tableView_ynab_data.setModel(self.data_ynab_model)

        self.ui.label_n_movements.setText(str(xls2ynab.n_movements))
        self.ui.label_account_number.setText(str(xls2ynab.account))
        self.ui.label_n_movements_without_payee.setText(str(xls2ynab.n_empty_payee))

    def payee_clicked(self, row, column):
        payee_id = self.ui.tableWidget_payees.item(row, 0)

        payee_name = self.ui.tableWidget_payees.currentItem().text()
        logger.debug(f"payee clicked {row}, {column}: {payee_name} ({payee_id.text()})")
        self.s_show_payee_keyword.emit(payee_name)









