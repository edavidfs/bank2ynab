import os
import sys
from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import logging
logger = logging.getLogger(__name__)
import pathlib


Base = declarative_base()


class DataBase:
    def __init__(self):
        database_folder = f"{pathlib.Path.home()}/.bank2ynab/"
        if not pathlib.Path(database_folder).exists():
            logger.debug("Folder not exist")
            os.mkdir(database_folder)
        self.engine = create_engine(f'sqlite:///{database_folder}database.db')
        logger.debug(f"DB engine: {self.engine}")
        Base.metadata.create_all(self.engine)
        DBSession = sessionmaker(bind=self.engine)
        self.session = DBSession()
        self.n_search = 0

    # def payee_for_description(self, description):
    #     output_payee = "test"
    #
    #     logger.debug(f"Searching Payee for Description: {description}")
    #     for word in description.split():
    #         payeekeyword = self.session.query(PayeeKeyword).filter(PayeeKeyword.keyword.ilike(f'{word}')).all()
    #         self.n_search += 1
    #         if payeekeyword:
    #             the_payeekeyword = payeekeyword[0]
    #             logger.debug(f"Find Payee {the_payeekeyword.keyword} for Description {word}")
    #             payee = self.session.query(Payee).filter(Payee.id == the_payeekeyword.payee_id).first()
    #             return payee.name
    #     return output_payee

    def payee_for_description(self, description):
        output_payee = ""
        logger.debug(f"Searching Payee for Description: {description}")
        payee_keyword_list = self.session.query(PayeeKeyword).all()
        self.n_search += 1
        for payee_keyword in payee_keyword_list:
            if payee_keyword.keyword.lower() in description.lower():
                payee = self.session.query(Payee).filter(Payee.id == payee_keyword.payee_id).first()
                self.n_search += 1
                return payee.name
        return output_payee

class Payee(Base):
    __tablename__ = 'payee'
    # Here we define columns for the table person
    # Notice that each column is also a normal Python instance attribute.
    id = Column(Integer, primary_key=True)
    name = Column(String(250), nullable=False)


class PayeeKeyword(Base):
    __tablename__ = 'payee_description'
    # Here we define columns for the table address.
    # Notice that each column is also a normal Python instance attribute.
    id = Column(Integer, primary_key=True)
    keyword = Column(String(250))
    payee_id = Column(Integer, ForeignKey('payee.id'))
    payees = relationship('Payee', backref='payee')


# Create an engine that stores data in the local directory's
# sqlalchemy_example.db file.
#engine = create_engine('sqlite:///bank2ynab.db')

# Create all tables in the engine. This is equivalent to "Create Table"
# statements in raw SQL.
#Base.metadata.create_all(engine)
