from pandas import DataFrame, read_excel, to_datetime
import logging
import xlrd
from datetime import datetime

logger = logging.getLogger(__name__)


class Xls2Ynab:

    def __init__(self, file_xls, database):

        self.memos = {}
        self.file_xls = file_xls
        self.data = DataFrame()
        self.database = database
        self.database.n_search = 0

        self.data_xls = read_excel(self.file_xls, skiprows=5)

        self.n_movements = self.data_xls.shape[0]
        logger.info(f'Num of entry: {self.n_movements}')

        self.output_file_name = ""
        self.account = ""
        self._read_data_account()

        self.n_empty_payee = 0
        self._convert_data()

    def _read_data_account(self):
        workbook = xlrd.open_workbook(self.file_xls)
        worksheet = workbook.sheet_by_index(0)

        self.account = worksheet.cell(1, 3).value
        logger.info(f"Account: {self.account}")

        start_date_cell_value = worksheet.cell_value(self.n_movements+5, 0)
        self.start_date = self._extrach_date_from_xls(start_date_cell_value, workbook.datemode)

        end_date_cell_value = worksheet.cell_value(6, 0)
        self.end_date = self._extrach_date_from_xls(end_date_cell_value, workbook.datemode)

        logger.info(f"Start date: {self.start_date} , end date: {self.end_date}")

        self.output_file_name = f"Datos {self.start_date} - {self.end_date} - cuenta {self.account}.csv"

    def _extrach_date_from_xls(self, xls_date, datemode):
        year, month, day, hour, minute, seconds = xlrd.xldate_as_tuple(xls_date, datemode)
        py_date = datetime(year, month, day)
        return py_date

    def _convert_data(self):
        self.data['Date'] = to_datetime(self.data_xls['F. VALOR'],  format='%d/%m/%Y')
        # self.data['Memo'] = dataframe_xls['DESCRIPCIÓN'].map(self._set_memo)
        self.data['Memo'] = self.data_xls['DESCRIPCIÓN']
        self.data['Inflow'] = self.data_xls['IMPORTE (€)'].map(self.set_inflow)
        self.data['Outflow'] = self.data_xls['IMPORTE (€)'].map(self.set_outflow)
        self.data['Payee'] = self.data_xls['DESCRIPCIÓN'].map(self.set_payee)
        self.data['Date'] = to_datetime(self.data['Date'], format='%d/%m/%Y')

        self.n_empty_payee = self.data['Payee'][self.data['Payee'] == ''].shape[0]

    def _set_memo(self, value):
        output_memo = value
        for memo in self.memos:
            if memo.lower() in value.lower():
                print("found new memo")
                output_memo = self.memos[memo]
                break
        return output_memo

    @staticmethod
    def set_inflow(value):
        output = 0
        if value > 0:
            output = value
        return output

    @staticmethod
    def set_outflow(value):
        output = 0
        if value < 0:
            output = -value
        return output

    def set_payee(self, movement_description):
        payee = self.database.payee_for_description(movement_description)
        logger.debug(f"Numeros de consultas: {self.database.n_search}")
        return payee

