# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'mainwindow.ui'
##
## Created by: Qt User Interface Compiler version 5.14.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import (QCoreApplication, QMetaObject, QObject, QPoint,
    QRect, QSize, QUrl, Qt)
from PySide2.QtGui import (QBrush, QColor, QConicalGradient, QFont,
    QFontDatabase, QIcon, QLinearGradient, QPalette, QPainter, QPixmap,
    QRadialGradient)
from PySide2.QtWidgets import *

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        if MainWindow.objectName():
            MainWindow.setObjectName(u"MainWindow")
        MainWindow.resize(769, 620)
        self.centralwidget = QWidget(MainWindow)
        self.centralwidget.setObjectName(u"centralwidget")
        self.gridLayout = QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout_3 = QGridLayout()
        self.gridLayout_3.setObjectName(u"gridLayout_3")

        self.gridLayout.addLayout(self.gridLayout_3, 1, 1, 1, 1)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")

        self.gridLayout.addLayout(self.horizontalLayout_2, 3, 1, 1, 1)

        self.gridLayout_2 = QGridLayout()
        self.gridLayout_2.setObjectName(u"gridLayout_2")

        self.gridLayout.addLayout(self.gridLayout_2, 0, 1, 1, 1)

        self.tabWidget = QTabWidget(self.centralwidget)
        self.tabWidget.setObjectName(u"tabWidget")
        font = QFont()
        font.setPointSize(10)
        self.tabWidget.setFont(font)
        self.tab_datos = QWidget()
        self.tab_datos.setObjectName(u"tab_datos")
        self.gridLayout_4 = QGridLayout(self.tab_datos)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.gridLayout_5 = QGridLayout()
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.label_5 = QLabel(self.tab_datos)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout_5.addWidget(self.label_5, 0, 0, 1, 1)

        self.label_6 = QLabel(self.tab_datos)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout_5.addWidget(self.label_6, 0, 1, 1, 1)

        self.tableView_ynab_data = QTableView(self.tab_datos)
        self.tableView_ynab_data.setObjectName(u"tableView_ynab_data")

        self.gridLayout_5.addWidget(self.tableView_ynab_data, 1, 1, 1, 1)

        self.tableView_xls_data = QTableView(self.tab_datos)
        self.tableView_xls_data.setObjectName(u"tableView_xls_data")

        self.gridLayout_5.addWidget(self.tableView_xls_data, 1, 0, 1, 1)


        self.gridLayout_4.addLayout(self.gridLayout_5, 4, 0, 1, 1)

        self.gridLayout_9 = QGridLayout()
        self.gridLayout_9.setObjectName(u"gridLayout_9")
        self.label_7 = QLabel(self.tab_datos)
        self.label_7.setObjectName(u"label_7")

        self.gridLayout_9.addWidget(self.label_7, 0, 0, 1, 1)

        self.label_account_number = QLabel(self.tab_datos)
        self.label_account_number.setObjectName(u"label_account_number")

        self.gridLayout_9.addWidget(self.label_account_number, 0, 1, 1, 1)

        self.label_n_movements = QLabel(self.tab_datos)
        self.label_n_movements.setObjectName(u"label_n_movements")

        self.gridLayout_9.addWidget(self.label_n_movements, 1, 1, 1, 1)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_9.addItem(self.horizontalSpacer, 0, 2, 1, 1)

        self.label_9 = QLabel(self.tab_datos)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout_9.addWidget(self.label_9, 1, 0, 1, 1)

        self.label_8 = QLabel(self.tab_datos)
        self.label_8.setObjectName(u"label_8")

        self.gridLayout_9.addWidget(self.label_8, 2, 0, 1, 1)

        self.label_n_movements_without_payee = QLabel(self.tab_datos)
        self.label_n_movements_without_payee.setObjectName(u"label_n_movements_without_payee")

        self.gridLayout_9.addWidget(self.label_n_movements_without_payee, 2, 1, 1, 1)


        self.gridLayout_4.addLayout(self.gridLayout_9, 2, 0, 1, 1)

        self.gridLayout_8 = QGridLayout()
        self.gridLayout_8.setObjectName(u"gridLayout_8")
        self.lineEdit_output_file = QLineEdit(self.tab_datos)
        self.lineEdit_output_file.setObjectName(u"lineEdit_output_file")

        self.gridLayout_8.addWidget(self.lineEdit_output_file, 2, 1, 1, 1)

        self.label_2 = QLabel(self.tab_datos)
        self.label_2.setObjectName(u"label_2")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_2.sizePolicy().hasHeightForWidth())
        self.label_2.setSizePolicy(sizePolicy)
        self.label_2.setMinimumSize(QSize(0, 30))
        self.label_2.setMaximumSize(QSize(16777215, 30))

        self.gridLayout_8.addWidget(self.label_2, 2, 0, 1, 1)

        self.label = QLabel(self.tab_datos)
        self.label.setObjectName(u"label")
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setMinimumSize(QSize(0, 30))
        self.label.setMaximumSize(QSize(16777215, 30))

        self.gridLayout_8.addWidget(self.label, 0, 0, 1, 1)

        self.lineEdit_input_file = QLineEdit(self.tab_datos)
        self.lineEdit_input_file.setObjectName(u"lineEdit_input_file")

        self.gridLayout_8.addWidget(self.lineEdit_input_file, 0, 1, 1, 1)

        self.pushButton_load_data = QPushButton(self.tab_datos)
        self.pushButton_load_data.setObjectName(u"pushButton_load_data")

        self.gridLayout_8.addWidget(self.pushButton_load_data, 0, 2, 1, 1)

        self.pushButton_export_data = QPushButton(self.tab_datos)
        self.pushButton_export_data.setObjectName(u"pushButton_export_data")

        self.gridLayout_8.addWidget(self.pushButton_export_data, 2, 2, 1, 1)


        self.gridLayout_4.addLayout(self.gridLayout_8, 0, 0, 1, 1)

        self.line_3 = QFrame(self.tab_datos)
        self.line_3.setObjectName(u"line_3")
        self.line_3.setFrameShape(QFrame.HLine)
        self.line_3.setFrameShadow(QFrame.Sunken)

        self.gridLayout_4.addWidget(self.line_3, 1, 0, 1, 1)

        self.pushButton_process_data = QPushButton(self.tab_datos)
        self.pushButton_process_data.setObjectName(u"pushButton_process_data")

        self.gridLayout_4.addWidget(self.pushButton_process_data, 5, 0, 1, 1)

        self.line_4 = QFrame(self.tab_datos)
        self.line_4.setObjectName(u"line_4")
        self.line_4.setFrameShape(QFrame.HLine)
        self.line_4.setFrameShadow(QFrame.Sunken)

        self.gridLayout_4.addWidget(self.line_4, 3, 0, 1, 1)

        self.tabWidget.addTab(self.tab_datos, "")
        self.tab_payees = QWidget()
        self.tab_payees.setObjectName(u"tab_payees")
        self.horizontalLayout = QHBoxLayout(self.tab_payees)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.splitter = QSplitter(self.tab_payees)
        self.splitter.setObjectName(u"splitter")
        self.splitter.setOrientation(Qt.Horizontal)
        self.frame = QFrame(self.splitter)
        self.frame.setObjectName(u"frame")
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setFrameShadow(QFrame.Raised)
        self.gridLayout_6 = QGridLayout(self.frame)
        self.gridLayout_6.setObjectName(u"gridLayout_6")
        self.label_3 = QLabel(self.frame)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout_6.addWidget(self.label_3, 0, 0, 1, 1)

        self.tableWidget_payees = QTableWidget(self.frame)
        self.tableWidget_payees.setObjectName(u"tableWidget_payees")

        self.gridLayout_6.addWidget(self.tableWidget_payees, 4, 0, 1, 1)

        self.lineEdit_new_payee = QLineEdit(self.frame)
        self.lineEdit_new_payee.setObjectName(u"lineEdit_new_payee")

        self.gridLayout_6.addWidget(self.lineEdit_new_payee, 2, 0, 1, 1)

        self.pushButton_add_payee = QPushButton(self.frame)
        self.pushButton_add_payee.setObjectName(u"pushButton_add_payee")

        self.gridLayout_6.addWidget(self.pushButton_add_payee, 3, 0, 1, 1)

        self.line = QFrame(self.frame)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.gridLayout_6.addWidget(self.line, 1, 0, 1, 1)

        self.pushButton_delete_payee = QPushButton(self.frame)
        self.pushButton_delete_payee.setObjectName(u"pushButton_delete_payee")

        self.gridLayout_6.addWidget(self.pushButton_delete_payee, 5, 0, 1, 1)

        self.splitter.addWidget(self.frame)
        self.frame_2 = QFrame(self.splitter)
        self.frame_2.setObjectName(u"frame_2")
        self.frame_2.setFrameShape(QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QFrame.Raised)
        self.gridLayout_7 = QGridLayout(self.frame_2)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.lineEdit_new_payee_keyword = QLineEdit(self.frame_2)
        self.lineEdit_new_payee_keyword.setObjectName(u"lineEdit_new_payee_keyword")

        self.gridLayout_7.addWidget(self.lineEdit_new_payee_keyword, 2, 0, 1, 1)

        self.tableWidget_payee_keywords = QTableWidget(self.frame_2)
        self.tableWidget_payee_keywords.setObjectName(u"tableWidget_payee_keywords")

        self.gridLayout_7.addWidget(self.tableWidget_payee_keywords, 4, 0, 1, 1)

        self.line_2 = QFrame(self.frame_2)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.HLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.gridLayout_7.addWidget(self.line_2, 1, 0, 1, 1)

        self.label_4 = QLabel(self.frame_2)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_7.addWidget(self.label_4, 0, 0, 1, 1)

        self.pushButton_add_payee_keyword = QPushButton(self.frame_2)
        self.pushButton_add_payee_keyword.setObjectName(u"pushButton_add_payee_keyword")

        self.gridLayout_7.addWidget(self.pushButton_add_payee_keyword, 3, 0, 1, 1)

        self.pushButton_delete_payee_keyword = QPushButton(self.frame_2)
        self.pushButton_delete_payee_keyword.setObjectName(u"pushButton_delete_payee_keyword")

        self.gridLayout_7.addWidget(self.pushButton_delete_payee_keyword, 5, 0, 1, 1)

        self.splitter.addWidget(self.frame_2)

        self.horizontalLayout.addWidget(self.splitter)

        self.tabWidget.addTab(self.tab_payees, "")

        self.gridLayout.addWidget(self.tabWidget, 0, 0, 1, 1)

        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(MainWindow)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 769, 22))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(MainWindow)
        self.statusbar.setObjectName(u"statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)

        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(MainWindow)
    # setupUi

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QCoreApplication.translate("MainWindow", u"MainWindow", None))
        self.label_5.setText(QCoreApplication.translate("MainWindow", u"Archivo de entrada (xls)", None))
        self.label_6.setText(QCoreApplication.translate("MainWindow", u"Archivo de salida (csv)", None))
        self.label_7.setText(QCoreApplication.translate("MainWindow", u"N\u00ba Cuenta:", None))
        self.label_account_number.setText(QCoreApplication.translate("MainWindow", u"-", None))
        self.label_n_movements.setText(QCoreApplication.translate("MainWindow", u"0", None))
        self.label_9.setText(QCoreApplication.translate("MainWindow", u"N\u00ba de movimientos:", None))
        self.label_8.setText(QCoreApplication.translate("MainWindow", u"N\u00ba de movimientos sin Payees", None))
        self.label_n_movements_without_payee.setText(QCoreApplication.translate("MainWindow", u"-", None))
        self.label_2.setText(QCoreApplication.translate("MainWindow", u"Output file (CVS)", None))
        self.label.setText(QCoreApplication.translate("MainWindow", u"Input file (XLS)", None))
        self.lineEdit_input_file.setPlaceholderText("")
        self.pushButton_load_data.setText(QCoreApplication.translate("MainWindow", u"Cargar XLS", None))
        self.pushButton_export_data.setText(QCoreApplication.translate("MainWindow", u"Exportar", None))
        self.pushButton_process_data.setText(QCoreApplication.translate("MainWindow", u"Procesar archivo de entrada", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_datos), QCoreApplication.translate("MainWindow", u"Datos", None))
        self.label_3.setText(QCoreApplication.translate("MainWindow", u"Payees", None))
        self.pushButton_add_payee.setText(QCoreApplication.translate("MainWindow", u"A\u00f1adir Payee", None))
        self.pushButton_delete_payee.setText(QCoreApplication.translate("MainWindow", u"Borrar Payee", None))
        self.label_4.setText(QCoreApplication.translate("MainWindow", u"Palabras claves", None))
        self.pushButton_add_payee_keyword.setText(QCoreApplication.translate("MainWindow", u"A\u00f1adir Palabra Clave", None))
        self.pushButton_delete_payee_keyword.setText(QCoreApplication.translate("MainWindow", u"Borrar Palabra Clave", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_payees), QCoreApplication.translate("MainWindow", u"Payees", None))
    # retranslateUi

