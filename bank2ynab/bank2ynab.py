import sys
import logging
from PySide2.QtWidgets import QApplication
from app.mainwindow import MainWindow
from app.app_controller import AppController


console_handler = logging.StreamHandler()

formatter = logging.Formatter('%(threadName)-25s - %(asctime)s - %(module)-20s - %(levelname)-6s - %(message)s')
console_handler.setFormatter(formatter)


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

logger.addHandler(console_handler)


if __name__ == '__main__':
    logger.info(f"Start bank2ynab App")

    app = QApplication(sys.argv)
    mainwindow = MainWindow()
    app_controller = AppController(mainwindow)
    mainwindow.show()
    sys.exit(app.exec_())
