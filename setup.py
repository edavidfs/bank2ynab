from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='bank2ynab',
    version='0.0.2',
    description='App to convert to bank xls data to ynab cvs',
    long_description=long_description,
    author='E. David Farina Santana',
    author_email='edavidfs',
    classifiers=[
        'Intended Audience :: Economics',
        'Topic :: Instrumentation Control',
        'Programming Language :: Python :: 3',
    ],
    keywords='ynab measurements',
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
)