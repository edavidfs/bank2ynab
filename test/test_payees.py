from bank2ynab.payees import Payees


def test_payees_instance():
    payees = Payees()
    assert isinstance(payees, Payees)


def test_payees_instance_list_empty():
    payees = Payees()
    assert len(payees.list) == 0


